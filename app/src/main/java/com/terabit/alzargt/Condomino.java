package com.terabit.alzargt;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by joshuamatus on 1/4/18.
 */

public class Condomino implements Parcelable {

    private String codigo;

    private String condominio;
    private String condominio_nombre;

    private String contador;

    private String contrasena;
    private String creador;

    private String correo;
    private Long fecha_creacion;
    private Long fechaultima_lectura;
    private String nombre;
    private String telefono;
    private Long ultimalectura_contador;

    public Condomino() {
    }


    public Condomino(String codigo, String condominio, String condominio_nombre,
                     String contador, String contrasena, String creador,
                     String correo, Long fecha_creacion, Long fechaultima_lectura,
                     String nombre, String telefono, Long ultimalectura_contador) {
        this.codigo = codigo;
        this.condominio = condominio;
        this.condominio_nombre = condominio_nombre;
        this.contador = contador;
        this.contrasena = contrasena;
        this.creador = creador;
        this.correo = correo;
        this.fecha_creacion = fecha_creacion;
        this.fechaultima_lectura = fechaultima_lectura;
        this.nombre = nombre;
        this.telefono = telefono;
        this.ultimalectura_contador = ultimalectura_contador;
    }

    public Condomino(HashMap<String,Object>condominoObject){
        if(condominoObject.get("codigo")!=null){
            if(condominoObject.get("codigo") instanceof Integer){
                this.codigo = String.valueOf((Integer) condominoObject.get("codigo"));
            }else if(condominoObject.get("codigo") instanceof Long ){
                this.codigo = String.valueOf((Long) condominoObject.get("codigo"));
            }
            else if(condominoObject.get("codigo") instanceof String ){
                this.codigo = (String) condominoObject.get("codigo");
            }
        }

        if(condominoObject.get("contador")!=null){
            if(condominoObject.get("contador") instanceof Integer){
                this.contador = String.valueOf((Integer) condominoObject.get("contador"));
            }else if(condominoObject.get("contador") instanceof Long ){
                this.contador = String.valueOf((Long) condominoObject.get("contador"));
            }
            else if(condominoObject.get("contador") instanceof String ){
                this.contador = (String) condominoObject.get("contador");
            }
        }

        if(condominoObject.get("correo")!=null){
            this.correo = (String)condominoObject.get("correo");
        }





        if(condominoObject.get("condominio")!=null){
            this.condominio = (String)condominoObject.get("condominio");
        }


        if(condominoObject.get("condominio_nombre")!=null){
            this.condominio_nombre = (String)condominoObject.get("condominio_nombre");
        }


        if(condominoObject.get("contrasena")!=null){
            this.contrasena = (String)condominoObject.get("contrasena");
        }


        if(condominoObject.get("creador")!=null){
            this.creador = (String)condominoObject.get("creador");
        }

        if(condominoObject.get("fecha_creacion")!=null){
            this.fecha_creacion = (Long)condominoObject.get("fecha_creacion");
        }






        if(condominoObject.get("fechaultima_lectura")!=null){
            this.fechaultima_lectura = (Long)condominoObject.get("fechaultima_lectura");
        }else{
            this.fechaultima_lectura = 0L;
        }

        if(condominoObject.get("nombre")!=null){
            this.nombre = (String)condominoObject.get("nombre");
        }


        if(condominoObject.get("telefono")!=null){
            if(condominoObject.get("telefono") instanceof Integer){
                this.telefono = String.valueOf((Integer) condominoObject.get("telefono"));
            }else if(condominoObject.get("telefono") instanceof Long ){
                this.telefono = String.valueOf((Long) condominoObject.get("telefono"));
            }
            else if(condominoObject.get("telefono") instanceof String ){
                this.telefono = (String) condominoObject.get("telefono");
            }
        }

        if(condominoObject.get("ultimalectura_contador")!=null){
            this.ultimalectura_contador = (Long) condominoObject.get("ultimalectura_contador");
        }else{
            this.ultimalectura_contador = 0L;
        }

    }

    protected Condomino(Parcel in) {
        codigo = in.readString();
        condominio = in.readString();
        condominio_nombre = in.readString();
        contador = in.readString();
        contrasena = in.readString();
        correo = in.readString();
        creador = in.readString();
        fecha_creacion = in.readLong();
        fechaultima_lectura = in.readLong();
        nombre = in.readString();
        telefono = in.readString();
        ultimalectura_contador = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(codigo);
        dest.writeString(condominio);
        dest.writeString(condominio_nombre);
        dest.writeString(contador);
        dest.writeString(contrasena);
        dest.writeString(correo);
        dest.writeString(creador);
        dest.writeLong(fecha_creacion);
        dest.writeLong(fechaultima_lectura);
        dest.writeString(nombre);
        dest.writeString(telefono);
        dest.writeLong(ultimalectura_contador);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Condomino> CREATOR = new Creator<Condomino>() {
        @Override
        public Condomino createFromParcel(Parcel in) {
            return new Condomino(in);
        }

        @Override
        public Condomino[] newArray(int size) {
            return new Condomino[size];
        }
    };

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getContador() {
        return contador;
    }

    public void setContador(String contador) {
        this.contador = contador;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Long getFechaultima_lectura() {
        return fechaultima_lectura;
    }

    public void setFechaultima_lectura(Long fechaultima_lectura) {
        this.fechaultima_lectura = fechaultima_lectura;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Long getUltimalectura_contador() {
        return ultimalectura_contador;
    }

    public void setUltimalectura_contador(Long ultimalectura_contador) {
        this.ultimalectura_contador = ultimalectura_contador;
    }


    public String getCondominio() {
        return condominio;
    }

    public void setCondominio(String condominio) {
        this.condominio = condominio;
    }

    public String getCondominio_nombre() {
        return condominio_nombre;
    }

    public void setCondominio_nombre(String condominio_nombre) {
        this.condominio_nombre = condominio_nombre;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getCreador() {
        return creador;
    }

    public void setCreador(String creador) {
        this.creador = creador;
    }

    public Long getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(Long fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
}
