package com.terabit.alzargt.Service;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.terabit.alzargt.R;
import com.terabit.alzargt.utils.Constans;
import com.terabit.alzargt.utils.Imagen;

import java.util.Calendar;
import java.util.HashMap;

public class ServiceCloudStorage extends Service {

    /*
     * Firebase Cloud storage
     * Referencias
     */

    StorageReference imageReference;
    StorageReference fileRef;
    static int progreso;
    static String url_callback;


    public ServiceCloudStorage() {
        super();
    } //contructor publico de la clase


    @Override
    public void onCreate() {
        super.onCreate();
    } //cuando se crea el servicio


    /**
     * Iniciar el servicio cuando se llamada a través del intent.
     *
     * @param intent  extras del intento
     * @param flags   banderas no se usaron
     * @param startId id para identificar la transferencia
     * @return START_REDELIVER_INTENT si el intento no funciono volver a inciciarlo en background o al iniciar la app
     */


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        String codigoContador = null; //iniciar código del contador

        if (intent != null) {
            codigoContador = intent.getStringExtra("codigocontador"); //obtener extra de codigo contador
            Uri fileUri = Uri.parse(intent.getStringExtra("data")); // obtener extra de la uri donde se encuentra el archivo
            final String uidContador = intent.getStringExtra("key_uid"); // obtener extra de la llave de fotos_lecturas

            Log.d(Constans.CLOUD_STORAGE, "uid_servicio" + uidContador);

            Calendar calendar = Calendar.getInstance();
            final int num_mes = calendar.get(Calendar.MONTH) + 1;// Aquí obtengo el dia del mes android tiene enero como 1 entonces se suma +1
            final int num_year = calendar.get(Calendar.YEAR);// Aquí obtengo el dia del año

            SharedPreferences preferences = getSharedPreferences(getResources().getString(R.string.pref_name), MODE_PRIVATE);
            final String condominio = preferences.getString("condominio", "condominio");
            //referencia a la carpeta en storage donde sera insertado.
            imageReference = FirebaseStorage.getInstance().getReference().child("contador").child(condominio).child(codigoContador).child("0" + num_mes + "-" + num_year);


            fileRef = imageReference.child("0" + num_mes + "-" + num_year + ".jpg"); // nombre del archivo en cloud storage
            final String finalCodigoContador = codigoContador;
            fileRef.putFile(fileUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            String name = taskSnapshot.getMetadata().getName();  //obtener nombre del archivo que se subio.
                            String url = taskSnapshot.getDownloadUrl().toString(); //obtener la url del archivo que se subio.

                            Log.d(Constans.CLOUD_STORAGE, "Uri: " + url);
                            Log.d(Constans.CLOUD_STORAGE, "Name: " + name);

                            if (url != null) { //cuando la url sea diferente ha null hacer la inserción
                                long millis = Calendar.getInstance().getTimeInMillis();
                                String namechild = "0" + num_mes + "-" + num_year;// formato 01-2018
                                writeNewContador(millis, url, uidContador, finalCodigoContador, namechild,condominio);
                                url_callback = url;
                            }


                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            Log.e(Constans.CLOUD_STORAGE, "error: " + exception);
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            Log.i(Constans.CLOUD_STORAGE, "subiendo: " + progress + "% "); //progreso de subida

                            progreso = (int) progress;
                            Log.e(Constans.CLOUD_STORAGE, ": " + progreso);
                        }
                    })
                    .addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {
                            Log.i(Constans.CLOUD_STORAGE, "subida pausada");
                        }
                    });
        }

        return START_REDELIVER_INTENT;// vuelve a la vida el servicio.


    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * inserción de datos a Firebase-Realtime
     *
     * @param fecha           fecha en que fue creado en milisegundos.
     * @param foto            la url generada despues de ser subido a firebase cloud storage
     * @param UID             llave de fotos_lecturas
     * @param numero_contador la llave del contador donde ira insertada la nueva lectura.
     * @param day_year        el mes y año para identificar la lectura
     */


    private void writeNewContador(long fecha, String foto, String UID, String numero_contador, String day_year,String uid_condominio) {

        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference(); // referencia a firebase realtime
        HashMap<String, Object> result = new HashMap<>();
        result.put("foto", foto);

      //  Imagen imagen = new Imagen(fecha, foto);
        mDatabase.child("lecturas").child(uid_condominio).child(numero_contador).child(day_year).updateChildren(result); //referencia a nodo de inserción

        Log.d("upload_file",uid_condominio+"/"+"/"+numero_contador+"/"+day_year+"/"+result);

        onDestroy();// Al finalizar transacción eliminar el servicio.


    }


    @Override
    public void onDestroy() { // destruir servicio
        Log.i(Constans.CLOUD_STORAGE, "servicio destruido");
        super.onDestroy();
    }


    public static int getProgreso() {
        return progreso;
    }

    public static String getUrl_callback() {
        return url_callback;
    }


}
