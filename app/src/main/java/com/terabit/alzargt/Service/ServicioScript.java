package com.terabit.alzargt.Service;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.terabit.alzargt.utils.Constans;
import com.terabit.alzargt.utils.Imagen;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ServicioScript extends Service {

    //inverso

    StorageReference imageReference;
    StorageReference fileRef;
    static int progreso;
    static String url_callback;

    private final static String NOMBRE_NODO = "fotos_lecturas";
    private final static String UID_MATCH = "-L06jTDKxW1YO3Xfda5L";
    private final static String UID_MES = "03-2018";


    public ServicioScript() {
    }


    @Override
    public void onCreate() {
        super.onCreate();
    } //cuando se crea el servicio


    /**
     * Iniciar el servicio cuando se llamada a través del intent.
     *
     * @param intent  extras del intento
     * @param flags   banderas no se usaron
     * @param startId id para identificar la transferencia
     * @return START_REDELIVER_INTENT si el intento no funciono volver a inciciarlo en background o al iniciar la app
     */


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        String codigoContador = null; //iniciar código del contador

        if (intent != null) {
            codigoContador = intent.getStringExtra("codigocontador"); //obtener extra de codigo contador
            //  Uri fileUri = Uri.parse(intent.getStringExtra("data")); // obtener extra de la uri donde se encuentra el archivo
            final String uidContador = intent.getStringExtra("key_uid"); // obtener extra de la llave de fotos_lecturas
            final long fechalong = intent.getLongExtra("fechalong", 1);


            Log.d(Constans.CLOUD_STORAGE, "uid_servicio" + uidContador);


            //referencia a la carpeta en storage donde sera insertado.
            imageReference = FirebaseStorage.getInstance().getReference().child("contador").child(codigoContador).child(UID_MES);


            fileRef = imageReference.child(UID_MES + ".jpg"); // nombre del archivo en cloud storage
            final String finalCodigoContador1 = codigoContador;

            try {
                Uri uri = Uri.parse("file:///storage/emulated/0/alz/" + codigoContador + UID_MES + ".jpg");
                fileRef.putFile(uri)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                String name = taskSnapshot.getMetadata().getName();  //obtener nombre del archivo que se subio.
                                String url = taskSnapshot.getDownloadUrl().toString(); //obtener la url del archivo que se subio.

                                Log.d(Constans.CLOUD_STORAGE, "Uri: " + url);
                                Log.d(Constans.CLOUD_STORAGE, "Name: " + name);

                                if (url != null) { //cuando la url sea diferente ha null hacer la inserción

                                    writeNewContadorFotoslecturas(url, finalCodigoContador1, fechalong);
                                    url_callback = url;
                                }


                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                Log.e(Constans.CLOUD_STORAGE, "error: " + exception);
                            }
                        })
                        .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                               // double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                               // Log.i(Constans.CLOUD_STORAGE, "subiendo: " + progress + "% "); //progreso de subida

                               // progreso = (int) progress;
                               // Log.e(Constans.CLOUD_STORAGE, ": " + progreso);
                            }
                        })
                        .addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {
                                Log.i(Constans.CLOUD_STORAGE, "subida pausada");
                            }
                        });
            } catch (Exception e) {
                Log.e(Constans.CLOUD_STORAGE, "error_subida: "+e);
            }

        }

        return START_REDELIVER_INTENT;// vuelve a la vida el servicio.


    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private void writeNewContadorFotoslecturas(String url, String codigoContador, long fecha) {

        Log.i(Constans.CLOUD_STORAGE, "INGRESO");

        DatabaseReference mDatabaseLecturas;
        mDatabaseLecturas = FirebaseDatabase.getInstance().getReference(); // referencia a firebase realtime



        Map<String,Object> taskMap = new HashMap<String,Object>();
        taskMap.put("fecha", fecha);
        taskMap.put("foto", url);

       // Imagen imagen = new Imagen(fecha, url);
        mDatabaseLecturas.child(NOMBRE_NODO).child(UID_MATCH).child(codigoContador).child(UID_MES).updateChildren(taskMap); //referencia a nodo de inserción


        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference(); // referencia a firebase realtime

      //  Imagen imagenFotos = new Imagen(fecha, url);
        mDatabase.child("lecturas").child(UID_MATCH).child(codigoContador).child(UID_MES).updateChildren(taskMap); //referencia a nodo de inserción


    }


    @Override
    public void onDestroy() { // destruir servicio
        Log.i(Constans.CLOUD_STORAGE, "servicio destruido");
        super.onDestroy();
    }


    public static int getProgreso() {
        return progreso;
    }

    public static String getUrl_callback() {
        return url_callback;
    }

}
