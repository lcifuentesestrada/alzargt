package com.terabit.alzargt;

import android.app.Application;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by joshuamatus on 1/4/18.
 */

public class AlzarApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);


        DatabaseReference condominosRef = FirebaseDatabase.getInstance().getReference("condominos");
        condominosRef.keepSynced(true);

        DatabaseReference lecturasRef = FirebaseDatabase.getInstance().getReference("lecturas");
        lecturasRef.keepSynced(true);
    }
}
