package com.terabit.alzargt;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ListadoLecturasActivity extends AppCompatActivity {

    private android.support.v7.widget.RecyclerView lecturaRV;
    private static final FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
    ArrayList<Lectura> mLecturas = new ArrayList<>();
    String condominio = "";
    String contador = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_lecturas);

        lecturaRV = (RecyclerView) findViewById(R.id.lecturaRV);

        StaggeredGridLayoutManager mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        lecturaRV.setLayoutManager(mStaggeredLayoutManager);

        SharedPreferences preferences = getSharedPreferences(getResources().getString(R.string.pref_name), MODE_PRIVATE);
        condominio = preferences.getString("condominio", "condominio");

        Bundle bundle = getIntent().getExtras();
        contador = bundle.getString("contador");


        try {

            DatabaseReference mReference;
            mReference = mDatabase.getReference();
            mReference.child("lecturas")
                    .child(condominio)
                    .child(contador)
                    .orderByChild("fecha")
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getValue() != null) {
                                for (DataSnapshot condominoSnapshot : dataSnapshot.getChildren()) {
                                    mLecturas.add(condominoSnapshot.getValue(Lectura.class));


                                }
                                upDateRV();
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void upDateRV() {
        LecturaAdapter lecturaAdapter = new LecturaAdapter(ListadoLecturasActivity.this, mLecturas);
        lecturaRV.setAdapter(lecturaAdapter);
    }
}
