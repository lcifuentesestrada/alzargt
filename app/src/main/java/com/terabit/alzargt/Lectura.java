package com.terabit.alzargt;

/**
 * Created by joshuamatus on 1/8/18.
 */

public class Lectura {
    private Long fecha;
    private String foto;
    private Long lectura;


    public Lectura(){}

    public Lectura(Long fecha, String foto, Long lectura) {
        this.fecha = fecha;
        this.foto = foto;
        this.lectura = lectura;
    }

    public Long getFecha() {
        return fecha;
    }

    public void setFecha(Long fecha) {
        this.fecha = fecha;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Long getLectura() {
        return lectura;
    }

    public void setLectura(Long lectura) {
        this.lectura = lectura;
    }
}
