package com.terabit.alzargt;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.terabit.alzargt.utils.Constans;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by joshuamatus on 1/8/18.
 */

public class LecturaAdapter extends RecyclerView.Adapter<LecturaAdapter.ViewHolder> {


    private static final String TAG = "detalle_lectura";
    Context mContext;
    private ArrayList<Lectura> mLecturas;
    LecturaAdapter.OnItemClickListener mItemClickListener;
    private static final String PICASSO = "picasso_debug";
    private static final int RESIZE = 512;
    private static final int RESIZEX = 512;

    public LecturaAdapter(Context mContext, ArrayList<Lectura> lecturas) {
        this.mContext = mContext;
        this.mLecturas = lecturas;


    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public CardView card;
        public LinearLayout mainHolder;


        private android.widget.TextView fechaLecturaTV;

        private android.widget.TextView valorLecturaTV;
        private ImageView lecturaIV;


        public ViewHolder(View itemView) {
            super(itemView);
            mainHolder = (LinearLayout) itemView.findViewById(R.id.mainHolder);
            card = (CardView) itemView.findViewById(R.id.condominoCard);
            fechaLecturaTV = (TextView) itemView.findViewById(R.id.fechaLecturaTV);
            valorLecturaTV = (TextView) itemView.findViewById(R.id.valorLecturaTV);
            lecturaIV = (ImageView) itemView.findViewById(R.id.lecturaIV);


            mainHolder.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(itemView, getPosition());


            }
        }
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final LecturaAdapter.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public int getItemCount() {
        return mLecturas.size();
    }

    @Override
    public LecturaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_lectura, parent, false);


        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.FROYO)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final LecturaAdapter.ViewHolder holder, final int position) {
        final Lectura lectura = mLecturas.get(position);

        try {
            if (lectura.getFecha() > 0 && lectura.getFecha() != null) {

                Log.i(TAG, lectura.getFecha().toString());

                if(lectura.getFecha() != null){
                    holder.fechaLecturaTV.setText(getTimeFromLong(lectura.getFecha()));
                }else {
                    holder.fechaLecturaTV.setText("0");
                }

            }

        }catch (Exception e){
            e.printStackTrace();
        }

        if (lectura.getLectura().toString() != null) {
            try {
                holder.valorLecturaTV.setText(lectura.getLectura().toString());
            } catch (Exception e) {
                e.printStackTrace();
            }


        }


//        Log.d(PICASSO, "" + lectura.getFoto().toString());


        try {
            //   if (lectura.getFoto().toString() != null) {

            if (lectura.getFoto().contains("firebasestorage")) {

                String url = lectura.getFoto();

                Glide
                        .with(mContext)
                        .load(url)
                        .apply(new RequestOptions()
                                .placeholder(R.mipmap.ic_launcher)
                                .fitCenter())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                Log.e(Constans.GLIDE, "Error de carga... " + e);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                Log.i(Constans.GLIDE, "ok... " + resource);
                                return false;
                            }
                        })
                        .into(holder.lecturaIV);


            } else {

                Log.d(PICASSO, "base64");

                final String encodedString = lectura.getFoto();
                final String pureBase64Encoded = encodedString.substring(encodedString.indexOf(",") + 1);
                final byte[] decodedBytes = Base64.decode(pureBase64Encoded, Base64.DEFAULT);

                Bitmap decodedImage = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);

                holder.lecturaIV.setImageBitmap(decodedImage);
            }
            //}


        } catch (Exception e) {
            e.printStackTrace();
        }


        //  if(lectura.getFoto().toString().contains("firebasestorage")){
        //     Glide.with(mContext).load(lectura.getFoto().toString()).into(holder.lecturaIV);




            /*
            Log.d(PICASSO, "storage");


            Picasso.with(mContext).setIndicatorsEnabled(true);

            Picasso.with(mContext)
                    .load(lectura.getFoto().toString())
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                   // .resize(RESIZE, RESIZE)
                  //  .onlyScaleDown()
                    .into(holder.lecturaIV, new Callback() {

                        @Override
                        public void onSuccess() {


                            Picasso.with(mContext)
                                    .load(lectura.getFoto().toString())
                                    .error(R.drawable.ic_file)
                                    //.resize(RESIZE, RESIZE)
                                   // .onlyScaleDown()
                                    .into(holder.lecturaIV);
                        }

                        @Override
                        public void onError() {


                            try {
                                Picasso.with(mContext)
                                        .load(lectura.getFoto().toString())
                                        .networkPolicy(NetworkPolicy.NO_CACHE)
                                        //.resize(RESIZE, RESIZE)
                                       // .onlyScaleDown()
                                        .into(holder.lecturaIV);
                            } catch (Exception e) {
                                Log.e(PICASSO, ": " + e);
                            }

                            Log.e(PICASSO, "No se pudo recuperar en cache");
                            Log.i(PICASSO, "Descargando...");
                        }
                    });
*/

        //      }

    }

    public static String getTimeFromLong(Long time) {
        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date(time);
        String mDate = formater.format(date);
        return mDate;
    }


}