package com.terabit.alzargt.utils;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

public class Code {

//
//    private android.widget.ImageView lecturaIV;
//    private android.widget.Button guardarLecturaBtn;
//    private String codigoContador = "";
//    String condomino_nombre = "nombre";
//    String imagen = "imagen";
//    Uri imagenURI;
//    Bitmap mBitmap;
//    Integer mContador = 0;
//
//    BluetoothAdapter mBluetoothAdapter;
//    BluetoothSocket mmSocket;
//    BluetoothDevice mmDevice;
//
//    // needed for communication to bluetooth device / network
//    OutputStream mmOutputStream;
//    InputStream mmInputStream;
//    Thread workerThread;
//
//    byte[] readBuffer;
//    int readBufferPosition;
//    volatile boolean stopWorker;
//
//    boolean isPrinterEnable = false;
//    boolean isPictureTaked = false;
//
//    private void openBT() {
//        try {
//
//            // Standard SerialPortService ID
//            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
//            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
//            mmSocket.connect();
//            mmOutputStream = mmSocket.getOutputStream();
//            mmInputStream = mmSocket.getInputStream();
//
//            beginListenForData();
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void beginListenForData() {
//        try {
//            final Handler handler = new Handler();
//
//            // this is the ASCII code for a newline character
//            final byte delimiter = 10;
//
//            stopWorker = false;
//            readBufferPosition = 0;
//            readBuffer = new byte[1024];
//
//            workerThread = new Thread(new Runnable() {
//                public void run() {
//
//                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {
//
//                        try {
//
//                            int bytesAvailable = mmInputStream.available();
//
//                            if (bytesAvailable > 0) {
//
//                                byte[] packetBytes = new byte[bytesAvailable];
//                                mmInputStream.read(packetBytes);
//
//                                for (int i = 0; i < bytesAvailable; i++) {
//
//                                    byte b = packetBytes[i];
//                                    if (b == delimiter) {
//
//                                        byte[] encodedBytes = new byte[readBufferPosition];
//                                        System.arraycopy(
//                                                readBuffer, 0,
//                                                encodedBytes, 0,
//                                                encodedBytes.length
//                                        );
//
//                                        // specify US-ASCII encoding
//                                        final String data = new String(encodedBytes, "US-ASCII");
//                                        readBufferPosition = 0;
//
//                                        // tell the user data were sent to bluetooth printer device
//                                        handler.post(new Runnable() {
//                                            public void run() {
//                                                Toast.makeText(MainActivity.this, "IMPRIMIENDO, POR FAVOR ESPERAR", Toast.LENGTH_LONG).show();
//
//                                            }
//                                        });
//
//                                    } else {
//                                        readBuffer[readBufferPosition++] = b;
//                                    }
//                                }
//                            }
//
//                        } catch (IOException ex) {
//                            stopWorker = true;
//                        }
//
//                    }
//                }
//            });
//
//            workerThread.start();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void findBT() {
//        try {
//            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//
//            if (mBluetoothAdapter == null) {
//                Toast.makeText(this, "NO SE ACCEDER AL BLUETOOTH", Toast.LENGTH_SHORT).show();
//
//            }
//
//            if (!mBluetoothAdapter.isEnabled()) {
//                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//                startActivityForResult(enableBluetooth, 0);
//            }
//
//
//            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
//
//            if (pairedDevices.size() > 0) {
//                for (BluetoothDevice device : pairedDevices) {
//
//                    // RPP300 is the name of the bluetooth printer device
//                    // we got this name from the list of paired devices
//                    if (device.getName().equals("BlueTooth Printer")) {
//                        mmDevice = device;
//                        isPrinterEnable = true;
//
//                        Toast.makeText(MainActivity.this, "DISPOSITIVO CONECTADO CON IMPRESORA", Toast.LENGTH_LONG).show();
//                        break;
//                    }
//                }
//            }
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        findBT();
//        openBT();
//
//
//    }
//
//
//    private void imprimirRecibo() {
//
//
//        String mensaje = condomino_nombre + "\n" + "___________________________" + "\n" +
//                "Comprobante No. " + mContador + "\n" +
//                "Fecha: " + getTimeFromLong(Calendar.getInstance().getTimeInMillis()) + "\n" +
//                "Numero de casa: " + "XXXXXX" + "\n\n" +
//                "Lectura Anterior: " + "XXXXXX" + "\n" +
//                "Lectura Actual: " + "XXXXXX" + "\n" +
//                "Total: " + "XXXXXX" + " m3" + "\n\n" +
//                "Cualquier consulta dirigirse\n a la Administracion\n";
//
//
//        try {
//            mmOutputStream.write(mensaje.getBytes());
//
//            if (mmOutputStream == null)
//                try {
//                    mmOutputStream = mmSocket.getOutputStream();
//                    stopWorker = true;
//                } catch (IOException e) {
//                    Log.e("imprimir_error", "error: " + e.getMessage());
//                }
//
//        } catch (Exception e) {
//
//            Toast.makeText(this, "ERROR AL INTENTAR IMPRIMIR", Toast.LENGTH_SHORT).show();
//            Log.e("imprimir_error", "error: " + e);
//            //finish();
//            mBluetoothAdapter.disable();
//            e.printStackTrace();
//        }
//
//      /*  if (mmSocket != null) {
//            try {
//                mmOutputStream.close();
//                mmSocket.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            mmSocket = null;
//        }*/
//
//    }
//
//    public static String getTimeFromLong(Long time) {
//        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyy");
//        Date date = new Date(time);
//        String mDate = formater.format(date);
//        return mDate;
//    }
//
//    public void btnPrint(View view) {
//        /*Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        };
//        Handler myHandler = new Handler();
//        myHandler.postDelayed(runnable, 1200);*/
//        imprimirRecibo();
//    }
}
