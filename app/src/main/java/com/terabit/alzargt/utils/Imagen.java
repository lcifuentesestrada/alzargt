package com.terabit.alzargt.utils;


public class Imagen {

    private long fecha; // tiempo en milisegundos
    private String foto; // url de la foto a insertar


    public Imagen(long fecha, String foto) {
        this.fecha = fecha;
        this.foto = foto;
    }

    public Imagen() {
    }

    public long getFecha() {
        return fecha;
    }

    public void setFecha(long fecha) {
        this.fecha = fecha;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
