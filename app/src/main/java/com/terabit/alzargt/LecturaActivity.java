package com.terabit.alzargt;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.commonsware.cwac.cam2.CameraActivity;
import com.commonsware.cwac.cam2.ZoomStyle;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.terabit.alzargt.Service.ServiceCloudStorage;
import com.terabit.alzargt.utils.Constans;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static com.terabit.alzargt.R.id.nameET;

public class LecturaActivity extends AppCompatActivity {

    private android.support.design.widget.TextInputEditText lecturaET;
    private android.widget.ImageView lecturaIV;
    private android.widget.Button guardarLecturaBtn;
    private String codigoContador = "";
    private Condomino mCondomino;
    String condomino_nombre = "nombre";
    String imagen = "imagen";
    Uri imagenURI;
    Bitmap mBitmap;
    Integer mContador = 0;
    boolean prnted = false;

    Long diferencia;

    private ProgressDialog mAuthProgressDialog, mUploadDialog;

    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;

    // needed for communication to bluetooth device / network
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;

    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;

    boolean isPrinterEnable = false;
    boolean isPictureTaked = false;
    String condominio = "";


    ///////////////////////////////////////////////////////////////////
    private StorageReference imageReference;
    private StorageReference fileRef;
    private Uri fileUri;
    int contador = 0;

    Query query;
    String key_uid;
    private String url_callback;

    private Handler handlers;
    private Runnable runnables;
    private final int runTimes = 500;

    private Long lectura_pasada;


    private void openBT() {

        if (mmOutputStream == null) {
            try {

                // Standard SerialPortService ID
                UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
                mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
                mmSocket.connect();
                mmOutputStream = mmSocket.getOutputStream();
                mmInputStream = mmSocket.getInputStream();

                beginListenForData();


            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "VOLVER A INTENTARLO", Toast.LENGTH_SHORT).show();
                finish();
                try {
                    new ObjectOutputStream(mmOutputStream);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                //findBT();
            }
        }

    }

    private void findBT() {
        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBluetoothAdapter == null) {
                Toast.makeText(this, "NO SE ACCEDER AL BLUETOOTH", Toast.LENGTH_SHORT).show();

            }

            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBluetooth, 0);
            }


            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {


                    if (device.getName().equals("BlueTooth Printer")) {
                        mmDevice = device;
                        isPrinterEnable = true;
                        Toast.makeText(LecturaActivity.this, "DISPOSITIVO CONECTADO CON IMPRESORA", Toast.LENGTH_LONG).show();
                        openBT();
                        break;
                    }
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void beginListenForData() {
        try {
            final Handler handler = new Handler();

            // this is the ASCII code for a newline character
            final byte delimiter = 10;

            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];

            workerThread = new Thread(new Runnable() {
                public void run() {

                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {

                        try {

                            int bytesAvailable = mmInputStream.available();

                            if (bytesAvailable > 0) {

                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);

                                for (int i = 0; i < bytesAvailable; i++) {

                                    byte b = packetBytes[i];
                                    if (b == delimiter) {

                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length
                                        );

                                        // specify US-ASCII encoding
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;

                                        handler.post(new Runnable() {
                                            public void run() {
                                                showToast();
                                            }
                                        });


                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }

                            }

                        } catch (IOException ex) {
                            stopWorker = true;
                        }

                    }

                }
            });

            workerThread.start();
            workerThread.isAlive();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (!prnted && mmSocket != null) {
            try {
                mmSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        prnted = false;

    }

    private void showToast() {
        Toast.makeText(this, "nuevo mensaje", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lectura);
        this.guardarLecturaBtn = (Button) findViewById(R.id.guardarLecturaBtn);
        this.lecturaIV = (ImageView) findViewById(R.id.lecturaIV);
        this.lecturaET = (TextInputEditText) findViewById(nameET);

        Bundle bundle = getIntent().getExtras();
        codigoContador = bundle.getString("codigo");

        SharedPreferences preferences = getSharedPreferences(getResources().getString(R.string.pref_name), MODE_PRIVATE);
        condominio = preferences.getString("condominio", "condominio");
        condomino_nombre = preferences.getString("condominio_nombre", "nombre");



        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                findBT();
            }
        };
        Handler myHandler = new Handler();
        myHandler.postDelayed(runnable, 500);


        //openBT();

//        final AlertDialog.Builder builder = new AlertDialog.Builder(LecturaActivity.this, R.style.CustomTheme_Dialog)
//                .setTitle("ATENCION")
//                .setMessage("Para continuar debes dar click en icono de la impresora")
//                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                })
//                .setIcon(android.R.drawable.ic_dialog_info)
//                .setCancelable(false);
//
//        AlertDialog alert = builder.create();
//        alert.show();

        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference mReference;
        mReference = mDatabase.getReference();
        mReference.child("correlativoVale")
                .child(condominio)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            mContador = dataSnapshot.getValue(Integer.class);

                        } else {
                            mContador = 0;
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


        Log.i("NODOS_INS", "lecturas/" + condominio + "/" + codigoContador + "/00-0000");


        getUltimaLectura(codigoContador);

        guardarLecturaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isPrinterEnable) {
                    Toast.makeText(LecturaActivity.this, "NO SE HA CONECTADO LA IMPRESORA", Toast.LENGTH_SHORT).show();
                    return;
                }

//                if(!isPictureTaked){
//                    Toast.makeText(LecturaActivity.this, "NO SE HA TOMADO LA FOTOGRAFIA DEL CONTADOR", Toast.LENGTH_SHORT).show();
//                    return;
//                }


                if (TextUtils.isEmpty(lecturaET.getText().toString())) {
                    lecturaET.setError("Por favor, ingresa el valor de la lectura");
                    lecturaET.requestFocus();
                    return;
                }

                Long lectura = Long.valueOf(lecturaET.getText().toString());

                if (lectura < mCondomino.getUltimalectura_contador()) {
                    lecturaET.setError("El valor de la lectura es menor al de la lectura pasada");
                    lecturaET.requestFocus();
                    return;
                }


                mAuthProgressDialog = new ProgressDialog(LecturaActivity.this);
                mAuthProgressDialog.setTitle("ESPERE");
                mAuthProgressDialog.setMessage("IMPRIMIENDO RECIBO");
                mAuthProgressDialog.setCancelable(false);
                mAuthProgressDialog.show();

                imprimirRecibo();


            }
        });

        lecturaIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                            checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                            checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                        showChooseSourceDialog();

                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(LecturaActivity.this, R.style.CustomTheme_Dialog)
                                .setTitle("ATENCION")
                                .setMessage("Para continuar debes habilitar los siguientes permisos.")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        showRequestPermission();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setCancelable(false);

                        AlertDialog alert = builder.create();
                        alert.show();


                    }

                } else {
                    showChooseSourceDialog();
                }
            }
        });


        mReference = FirebaseDatabase.getInstance().getReference();
        mReference.child("condominos")
                .child(condominio)
                .orderByChild("contador")
                .equalTo(codigoContador)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                HashMap<String, Object> condomino = (HashMap<String, Object>) snapshot.getValue();
                                mCondomino = new Condomino(condomino);
                            }

                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


    }


    private void postReciboInFirebase() {

        prnted = true;
        Long lectura = Long.valueOf(lecturaET.getText().toString());

        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference reference = mDatabase.getReference("lecturas")
                .child(condominio)
                .child(codigoContador)
                .child(getMonthAndYear());

        HashMap<String, Object> lecturaContador = new HashMap<String, Object>();
        lecturaContador.put("fecha", Calendar.getInstance().getTimeInMillis());
        lecturaContador.put("lectura", lectura);

        if (lectura_pasada != null) {
            lecturaContador.put("lectura_pasada", lectura_pasada);//mCondomino.getUltimalectura_contador());

        } else {
            lecturaContador.put("lectura_pasada", "0");//mCondomino.getUltimalectura_contador());
        }

        lecturaContador.put("fecha_pasada", mCondomino.getFechaultima_lectura());
        lecturaContador.put("no_boleta", mContador);

        mCondomino.setFechaultima_lectura(Calendar.getInstance().getTimeInMillis());
        mCondomino.setUltimalectura_contador(lectura);


        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        mBitmap.compress(Bitmap.CompressFormat.WEBP, 0, baos);


        //TODO AQUI OBTENER URL NUEVA

        reference.setValue(lecturaContador);


        reference = mDatabase.getReference("condominos")
                .child(condominio)
                .child(condominio + mCondomino.getCodigo());

        reference.setValue(mCondomino);


        reference = mDatabase.getReference("correlativoVale")
                .child(condominio);
        reference.setValue(mContador + 1);


        try {
            mmSocket.close();
            mAuthProgressDialog.dismiss();
        } catch (Exception e) {
            Log.d(Constans.CLOUD_STORAGE, "" + e);
        }
        finish();
    }

    private void imprimirRecibo() {


        Long lectura = Long.valueOf(lecturaET.getText().toString());


        String mayusculas_nombre = condomino_nombre.toUpperCase();
        String imp;

        if (lectura_pasada != null) {
            imp = String.valueOf(lectura_pasada);
            diferencia = lectura - lectura_pasada;

        } else {
            imp = "0";
            diferencia = 0L;

        }


        String mensaje = mayusculas_nombre + "\n" + "___________________________" + "\n" +
                "Comprobante No. " + mContador + "\n" +
                "Fecha: " + getTimeFromLong(Calendar.getInstance().getTimeInMillis()) + "\n" +
                "Numero de casa: " + mCondomino.getCodigo() + "\n\n" +
                "Lectura Anterior: " + imp + "\n" +
                "Lectura Actual: " + lectura + "\n" +
                "Total: " + diferencia + " m3" + "\n\n" +
                "CUALQUIER CONSULTA DIRIGIRSE\n A LA ADMINISTRACION\n\n\n\n\n\n";
        try {
            mmOutputStream.write(mensaje.getBytes());
            Runnable runnable = new Runnable() {
                @Override
                public void run() {

                    Log.d(Constans.CLOUD_STORAGE, "todo ok");
                    uploadImage();

                    mAuthProgressDialog.dismiss();


                }
            };


            postReciboInFirebase();


        } catch (
                Exception e)

        {
            Toast.makeText(this, "ERROR AL INTENTAR IMPRIMIR", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }


    }

    private void uploadImage() {



        /*
         * Obtener_uid de foto_lectura firebase-realtime
         */

        SharedPreferences prefsUriv = getSharedPreferences(Constans.URI_CLASS, Context.MODE_PRIVATE);
        final String URIv = prefsUriv.getString(Constans.URI_KEY, null);

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        query = reference.child("fotos_lecturas"); //referencia a nodo
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        Log.i(Constans.CLOUD_STORAGE, "key: " + issue.getKey()); //obtener uid

                        key_uid = issue.getKey();

                        if (key_uid != null) { // si la uid es diferente de null seguir.
                            // Agregar a cola de servicio.


                            Intent i = new Intent(LecturaActivity.this, ServiceCloudStorage.class);
                            i.putExtra("data", URIv); // enviar uri
                            i.putExtra("codigocontador", codigoContador); // enviar codigo de contador.
                            i.putExtra("key_uid", key_uid); // enviar uid del nodo a insertar
                            startService(i);


                        }

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i(Constans.CLOUD_STORAGE, "se cancelo");
            }
        });


    }

    public static String getTimeFromLong(Long time) {
        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyy");
        Date date = new Date(time);
        String mDate = formater.format(date);
        return mDate;
    }

    private String getMonthAndYear() {
        SimpleDateFormat formater = new SimpleDateFormat("MM-yyyy");
        Date date = new Date(Calendar.getInstance().getTimeInMillis());
        String mDate = formater.format(date);
        return mDate;
    }

    private void showRequestPermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {

        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showStoragePermission();

            } else {
                showNoPermission();
                //Toast.makeText(SigninActivity.this, "NO PERMISSION", Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == 2) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                showChooseSourceDialog();

            } else {
                showNoStoragePermission();
                //Toast.makeText(SigninActivity.this, "NO PERMISSION", Toast.LENGTH_SHORT).show();
            }
        }


    }


    private void showNoStoragePermission() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LecturaActivity.this, R.style.CustomTheme_Dialog)
                .setTitle("ATENCION")
                .setMessage("No se puede continuar sin este permiso.")
                .setPositiveButton("Dar Permiso", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
                        }
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info);

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showStoragePermission() {

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        }

    }

    private void showNoPermission() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(LecturaActivity.this, R.style.CustomTheme_Dialog)
                .setTitle("ATENCION")
                .setMessage("No se puede continuar el proceso sin este permiso.")
                .setPositiveButton("Dar Permiso", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA}, 1);
                        }
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .setCancelable(false);

        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        ///////////////////////////////////////////////////
        if (data != null) {// verificar si trae una uri en data.


            //obtener uri del archivo
            Uri uri = data.getData();
            SharedPreferences prefs = getSharedPreferences(Constans.URI_CLASS, Context.MODE_PRIVATE);

            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(Constans.URI_KEY, String.valueOf(uri));
            editor.apply();

        }
        ///////////////////////////////////////////////////

        try {
            if (requestCode == 1 && resultCode == RESULT_OK) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }

                Uri selectedImageUri;
                if (isCamera && data != null) { // if there is no picture
                    return;
                }
                if (isCamera || data == null || data.getData() == null) {
                    selectedImageUri = outputFileUri;
                } else {
                    selectedImageUri = data == null ? null : data.getData();
                }
                imagenURI = selectedImageUri;
                imagen = selectedImageUri.toString();
                isPictureTaked = true;

                InputStream mInputStream = null;
                try {
                    mInputStream = this.getContentResolver().openInputStream(selectedImageUri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                bmpFactoryOptions.inJustDecodeBounds = false;
                bmpFactoryOptions.inSampleSize = 8;

                Bitmap bitmap = BitmapFactory.decodeStream(mInputStream, null, bmpFactoryOptions);
                try {
                    mInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                mBitmap = bitmap;


                lecturaIV.setImageBitmap(bitmap);
            }


        } catch (RuntimeException exception) {
            exception.printStackTrace();
        }


        ///////////////////////////////////////////////////
        if (requestCode == 0 && resultCode == RESULT_OK) {
            finish();
        }
        if (requestCode == 0 && resultCode == RESULT_CANCELED) {
            finish();
        }
        //////////////////////////////////////////////////

    }


    private File file;
    private Uri outputFileUri;

    private void showChooseSourceDialog() {

        prnted = true;


        final File mediaStorageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        final File root = new File(mediaStorageDir, "AlzarGT");
        root.mkdirs();
        final String fname = "img_" + System.currentTimeMillis() + ".jpg";
        file = new File(root, fname);
        outputFileUri = Uri.fromFile(file);
        Intent takePhotoIntent = new CameraActivity.IntentBuilder(LecturaActivity.this)
                .skipConfirm()
                .to(this.file)
                .zoomStyle(ZoomStyle.SEEKBAR)
                .updateMediaStore()
                .build();

        Intent pickPhotoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhotoIntent.setType("image/*");
        Intent chooserIntent = Intent.createChooser(pickPhotoIntent, "Toma una foto o escogela de galeria");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{takePhotoIntent});
        try {
            startActivityForResult(chooserIntent, 1);
        } catch (RuntimeException exception) {
            Log.d("debug", exception.getMessage());
            exception.printStackTrace();
        }


    }

    private void getUltimaLectura(String cdContador) {


        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference mReference;
        mReference = mDatabase.getReference();

        //String mesVerificar = getMesPasado();


        mReference.child("lecturas")
                .child(condominio)
                .child(cdContador)
                .child(getMesPasado())
                // .child("01-2018")// prueba
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d("NODOS_INS", "" + dataSnapshot);
                        if (dataSnapshot.getValue() != null) {


                            try {

                                Map<Long, Object> map = (Map<Long, Object>) dataSnapshot.getValue();
                                lectura_pasada = (Long) map.get("lectura");
                                Log.i("NODE_INS", "" + lectura_pasada);
                            } catch (Exception e) {
                                Map<Double, Object> map = (Map<Double, Object>) dataSnapshot.getValue();
                                Double s = (Double) map.get("lectura");
                                lectura_pasada = s.longValue();
                                Log.i("NODE_INS", "" + lectura_pasada);
                            }


                        } else {
                            lectura_pasada = null;
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }


    private String getMesPasado() {
        Integer mes_actual = Calendar.getInstance().get(Calendar.MONTH) + 1;
        Integer year = Calendar.getInstance().get(Calendar.YEAR);
        Log.d("NODOS_INS", "0" + (mes_actual - 1) + "-" + year);
        return "0" + (mes_actual - 1) + "-" + year;
    }


}