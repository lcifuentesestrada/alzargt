package com.terabit.alzargt;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRReaderActivity extends Activity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;

    private static final int PERMISSION_REQUEST_CAMERA = 1;

    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.CAMERA,
            Manifest.permission.VIBRATE,
    };



    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);

        if(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1){

            int cameraPermission = ActivityCompat.checkSelfPermission(QRReaderActivity.this, PERMISSIONS_STORAGE[0]);
            int vibratermission = ActivityCompat.checkSelfPermission(QRReaderActivity.this, PERMISSIONS_STORAGE[1]);

            if (cameraPermission != PackageManager.PERMISSION_GRANTED || vibratermission != PackageManager.PERMISSION_GRANTED) {
                // We don't have permission so prompt the user
                ActivityCompat.requestPermissions(
                        QRReaderActivity.this,
                        PERMISSIONS_STORAGE,
                        PERMISSION_REQUEST_CAMERA);

            }


        }

        mScannerView = new ZXingScannerView(this);
        mScannerView.setAutoFocus(true);
        // Programmatically initialize the scanner view
        setContentView(mScannerView);                // Set the scanner view as the content view
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        v.vibrate(500);
        putResult(rawResult.getText());

        // If you would like to resume scanning, call this method below:
        mScannerView.resumeCameraPreview(this);
    }

    private void putResult(String code) {
        Intent intent = new Intent();
        Bundle args = new Bundle();
        args.putString("QRCODE",code);
        intent.putExtras(args);
        setResult(RESULT_OK,intent);
        finish();
    }
}