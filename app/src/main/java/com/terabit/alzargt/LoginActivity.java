package com.terabit.alzargt;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {

    EditText mailET, passwordET;
    private ProgressDialog mAuthProgressDialog;
    private FirebaseAuth mAuth;
    private static final FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
    private FirebaseAuth.AuthStateListener mAuthListener;
    private static final String TAG = "Login";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mailET = (EditText) findViewById(R.id.mailET);
        passwordET = (EditText) findViewById(R.id.passwordET);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {

                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }

            }
        };
    }

    public void btnForgotPassword(View view) {
        if (mailET.getText().toString().equals("")) {
            mailET.setError("Por favor, ingresa un correo electrónico");
            return;
        }
        FirebaseAuth.getInstance().sendPasswordResetEmail(mailET.getText().toString())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this, R.style.CustomTheme_Dialog)
                                    .setTitle("ATENCION")
                                    .setMessage("Por favor revisa tu correo, un link de recuperación se ha generado")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            dialog.dismiss();
                                        }
                                    });

                            AlertDialog alert = builder.create();
                            alert.show();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this, R.style.CustomTheme_Dialog)
                                    .setTitle("ATENCION")
                                    .setMessage("No existe una cuenta con este correo.")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            dialog.dismiss();
                                        }
                                    });

                            AlertDialog alert = builder.create();
                            alert.show();

                        }
                    }
                });


    }

    public void btnLoginPressed(View view) {
        mAuthProgressDialog = new ProgressDialog(this);
        mAuthProgressDialog.setTitle("CONECTANDO AL SERVIDOR");
        mAuthProgressDialog.setMessage("Iniciando Sesión");
        mAuthProgressDialog.setCancelable(false);

        String email = mailET.getText().toString().toLowerCase();
        String password = passwordET.getText().toString();

        String correo = email.replace(" ", "");
        if (isEmailValid(correo) && isPasswordValid(password)) {

            mAuthProgressDialog.show();
            mAuth.signInWithEmailAndPassword(correo, password)
                    .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {


                            if (!task.isSuccessful()) {
                                Toast.makeText(LoginActivity.this, "NO SE PUDO INICIAR SESION",
                                        Toast.LENGTH_SHORT).show();
                                mAuthProgressDialog.dismiss();
                            } else {


//                                mUser.getIdToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
//                                    @Override
//                                    public void onSuccess(GetTokenResult result) {
//                                        String idToken = result.getToken();
//                                        //Do whatever
//                                        Log.d(TAG, "GetTokenResult result = " + idToken);
//                                        String[] splitStr =  idToken.split(".");
//
//                                        byte[] data = Base64.decode(splitStr[0], Base64.DEFAULT);
//                                        String text = null;
//                                        try {
//                                            text = new String(data, "UTF-8");
//                                            Log.d(TAG, "onSuccess: "+text);
//                                        } catch (UnsupportedEncodingException e) {
//                                            e.printStackTrace();
//                                        }
//
//                                        //HashMap<String,Object> payload = Base64.decode(idToken, Base64.DEFAULT);
//                                    }
//                                });

                                SharedPreferences.Editor sharedPrefEditor =
                                        getApplicationContext()
                                                .getSharedPreferences(getResources().getString(R.string.pref_name), MODE_PRIVATE)
                                                .edit();
                                sharedPrefEditor.putBoolean("loged", true);
                                sharedPrefEditor.apply();

                                DatabaseReference mReference;
                                mReference = mDatabase.getReference();
                                mReference.child("usuarios")
                                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                if (dataSnapshot.getValue() != null) {
                                                    HashMap<String, Object> mUser = (HashMap<String, Object>) dataSnapshot.getValue();
                                                    if (mUser.get("tipo") != null) {

                                                        String tipo = (String) mUser.get("tipo");


                                                        if (tipo.equals("administrador")) {

                                                            SharedPreferences.Editor sharedPrefEditor =
                                                                    getApplicationContext()
                                                                            .getSharedPreferences(getResources().getString(R.string.pref_name), MODE_PRIVATE)
                                                                            .edit();
                                                            sharedPrefEditor.putString("condominio", (String) mUser.get("condominio"));
                                                            sharedPrefEditor.putString("condominio_nombre", (String) mUser.get("condominio_nombre"));
                                                            sharedPrefEditor.putString("foto", (String) mUser.get("foto"));
                                                            sharedPrefEditor.putString("nombre", (String) mUser.get("nombre"));

                                                            Log.d("AZAZELXT", "" + mUser.get("condominio"));
                                                            Log.d("AZAZELXT", "" + mUser.get("condominio_nombre"));
                                                            Log.d("AZAZELXT", "" + mUser.get("foto"));
                                                            Log.d("AZAZELXT", "" + mUser.get("nombre"));

                                                            sharedPrefEditor.apply();
                                                            mAuthProgressDialog.dismiss();
                                                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                                            startActivity(intent);
                                                            finish();
                                                        } else {
                                                            mAuthProgressDialog.dismiss();
                                                            Toast.makeText(LoginActivity.this, "NO SE CUENTAN CON LOS PERMISOS NECESARIOS", Toast.LENGTH_SHORT).show();
                                                        }


                                                    }

                                                }

                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });


//                                mAuthProgressDialog.dismiss();
//                                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
//                                startActivity(intent);
//                                finish();


                            }
                        }
                    });

        }

    }

    private void getUserInfo() {
        DatabaseReference mReference;
        mReference = mDatabase.getReference();
        mReference.child("usuarios")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        HashMap<String, Object> user = (HashMap<String, Object>) dataSnapshot.getValue();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private boolean isEmailValid(String email) {

        if (email.equals("")) {
            mailET.setError("Por favor, ingresa un correo electrónico");
            return false;
        }
        boolean isGoodEmail =
                (email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches());
        if (!isGoodEmail) {
            mailET.setError(String.format(getString(R.string.error_invalid_email_not_valid),
                    email));
            return false;
        }


        return isGoodEmail;
    }


    private boolean isPasswordValid(String password) {

        if (password.equals("")) {
            passwordET.setError(getResources().getString(R.string.error_invalid_password_not_valid));
            return false;
        }
        if (password.length() < 6) {
            passwordET.setError(getResources().getString(R.string.error_invalid_password_not_valid));
            return false;
        }
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}