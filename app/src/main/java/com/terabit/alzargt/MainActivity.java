package com.terabit.alzargt;

import android.app.AlertDialog;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.InputType;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private android.support.v7.widget.RecyclerView condominosRV;
    private static final FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
    ArrayList<Condomino>mCondominos = new ArrayList<>();
    EditText codigoInput;
    String condominio = "";
    boolean isContador = false;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.logout) {

            FirebaseAuth.getInstance().signOut();
            SharedPreferences.Editor sharedPrefEditor =
                    getApplicationContext()
                            .getSharedPreferences(getResources().getString(R.string.pref_name),MODE_PRIVATE)
                            .edit();

            sharedPrefEditor.putBoolean("loged", false);

            sharedPrefEditor.apply();



            Intent intent = new Intent(MainActivity.this,LoginActivity.class);
            startActivity(intent);

            finish();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);






        this.condominosRV = (RecyclerView) findViewById(R.id.condominosRV);

        StaggeredGridLayoutManager mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        condominosRV.setLayoutManager(mStaggeredLayoutManager);

        SharedPreferences preferences = getSharedPreferences(getResources().getString(R.string.pref_name), MODE_PRIVATE);
        condominio = preferences.getString("condominio","condominio");

        FloatingActionButton newLectureBtn = (FloatingActionButton) findViewById(R.id.newLectureBtn);
        newLectureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Ingresar numero de contador");

                final GestureDetector mGestureDetector = new GestureDetector(MainActivity.this, new GestureDetector.SimpleOnGestureListener() {

                    @Override
                    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                                           float velocityY) {

                        return false;
                    }

                });

                mGestureDetector.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {
                    @Override
                    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
                        return false;
                    }

                    @Override
                    public boolean onDoubleTap(MotionEvent motionEvent) {
                        startQRActivity();
                        return false;
                    }

                    @Override
                    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
                        return false;
                    }
                });

                codigoInput = new EditText(MainActivity.this);
                codigoInput.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        mGestureDetector.onTouchEvent(motionEvent);
                        return false;
                    }
                });

                codigoInput.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(codigoInput);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if(!(codigoInput.getText().toString().equals(""))){
                            codigoInput.clearFocus();
                            String codigo = (codigoInput.getText().toString());

                            String codigoMayusculas = codigo.toUpperCase();

                            InputMethodManager imm = (InputMethodManager)MainActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(codigoInput.getWindowToken(), 0);


                            searchCode(codigoMayusculas);
                        }else{
                            codigoInput.setError("No se ha ingresado ningun valor");
                            return;
                        }

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });

        DatabaseReference mReference;
        mReference = mDatabase.getReference();
        mReference.child("condominos")
                .child(condominio)
                .orderByChild("codigo")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getValue()!=null){
                            for(DataSnapshot condominoSnapshot:dataSnapshot.getChildren()){
                                HashMap<String,Object>condomino = (HashMap<String, Object>) condominoSnapshot.getValue();
                                mCondominos.add(new Condomino(condomino));


                            }
                            upDateRV();
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });



    }



    private void searchCode(final String codigo) {

        if (isContador){
            DatabaseReference mReference;
            mReference = mDatabase.getReference();
            mReference.child("condominos")
                    .child(condominio)
                    .orderByChild("contador")
                    .equalTo(codigo)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot.getValue()!=null){
                                verifyLastDate(codigo);
                            }else{
                                Toast.makeText(MainActivity.this, "NUMERO DE CONTADOR INVALIDO", Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
        }else{
            DatabaseReference mReference;
            mReference = mDatabase.getReference();
            mReference.child("condominos")
                    .child(condominio)
                    .orderByChild("codigo")
                    .equalTo(codigo)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot.getValue()!=null){
                                HashMap<String,Object> condominoObject = (HashMap)dataSnapshot.getValue();
                                Condomino condomino = new Condomino((HashMap)condominoObject.get(condominio+codigo));
                                verifyLastDate(condomino.getContador());
                            }else{
                                Toast.makeText(MainActivity.this, "NUMERO DE CASA INVALIDO", Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

        }





    }

    private void verifyLastDate(final String codigo) {
        DatabaseReference mReference;
        mReference = mDatabase.getReference();
        mReference.child("lecturas")
                .child(condominio)
                .child(codigo)
                .child(getMonthAndYear())
                .child("lectura")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getValue()==null){
                            Intent intent = new Intent(MainActivity.this,LecturaActivity.class);
                            intent.putExtra("codigo",codigo);
                            startActivity(intent);
                        }else{
                            Toast.makeText(MainActivity.this, "ESTE CONTADOR YA TIENE UNA LECTURA DE ESTE MES", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private String getMonthAndYear() {
        SimpleDateFormat formater = new SimpleDateFormat("MM-yyyy");
        Date date = new Date(Calendar.getInstance().getTimeInMillis());
        String mDate = formater.format(date);
        return mDate;
    }

    private void startQRActivity() {
        Intent intent = new Intent(MainActivity.this,QRReaderActivity.class);
        startActivityForResult(intent,0);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==0 && resultCode == RESULT_OK) {
            Bundle args = data.getExtras();
            final String code = args.getString("QRCODE");
            codigoInput.setText(code);
            isContador = true;
        }
    }

    private void upDateRV() {
        CondominosAdapter condominosAdapter = new CondominosAdapter(MainActivity.this, mCondominos);
        condominosRV.setAdapter(condominosAdapter);
        condominosAdapter.setOnItemClickListener(onItemClickListener);
    }

    CondominosAdapter.OnItemClickListener onItemClickListener = new CondominosAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
            Intent intent = new Intent(MainActivity.this,ListadoLecturasActivity.class);
            intent.putExtra("contador",mCondominos.get(position).getContador());
            startActivity(intent);
        }
    };
}