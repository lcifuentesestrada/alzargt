package com.terabit.alzargt;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by joshuamatus on 1/4/18.
 */

public class CondominosAdapter extends RecyclerView.Adapter<CondominosAdapter.ViewHolder>{


    Context mContext;
    private ArrayList<Condomino> mCondominos;
    CondominosAdapter.OnItemClickListener mItemClickListener;




    public CondominosAdapter(Context mContext, ArrayList<Condomino> Condominos) {
        this.mContext = mContext;
        this.mCondominos = Condominos;


    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {



        public CardView card;
        public LinearLayout mainHolder;


        private android.widget.TextView numeroTV;
        private android.widget.TextView fechaLecturaTV;
        private android.widget.TextView nombreTV;
        private android.widget.TextView valorLecturaTV;;
        private ImageView CondominoIV;


        public ViewHolder(View itemView) {
            super(itemView);
            mainHolder = (LinearLayout) itemView.findViewById(R.id.mainHolder);
            card = (CardView)itemView.findViewById(R.id.condominoCard);
            numeroTV = (TextView)itemView.findViewById(R.id.numeroTV);
            fechaLecturaTV = (TextView)itemView.findViewById(R.id.fechaLecturaTV);
            nombreTV = (TextView)itemView.findViewById(R.id.nombreTV);
            valorLecturaTV = (TextView)itemView.findViewById(R.id.valorLecturaTV);



            mainHolder.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(itemView, getPosition());


            }
        }
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final CondominosAdapter.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
    @Override
    public int getItemCount() {
        return mCondominos.size();
    }

    @Override
    public CondominosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_condomino , parent, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CondominosAdapter.ViewHolder holder, final int position) {
         Condomino condomino = mCondominos.get(position);


        holder.nombreTV.setText(condomino.getNombre());
        holder.numeroTV.setText("Condomino \n#"+condomino.getCodigo());
        if(condomino.getFechaultima_lectura()>0){
            holder.fechaLecturaTV.setText("Ult. Lectura: \n"+getTimeFromLong(condomino.getFechaultima_lectura()));
        }
        holder.valorLecturaTV.setText("Valor Lectura: \n"+condomino.getUltimalectura_contador());





    }

    public static String  getTimeFromLong(Long time) {
        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date(time);
        String mDate = formater.format(date);
        return mDate;
    }




}
