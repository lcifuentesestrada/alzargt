package com.terabit.alzargt.script;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.terabit.alzargt.Service.ServicioScript;
import com.terabit.alzargt.utils.Constans;
import com.terabit.alzargt.utils.Imagen;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by cifue on 02/04/2018.
 */

public class Listas {

    //inverso
    private Query nUno, nDos;
    StorageReference imageReference;
    StorageReference fileRef;
    Context ctx;

    private final static String NOMBRE_NODO = "fotos_lecturas";
    private final static String UID_MATCH = "-L06jTDKxW1YO3Xfda5L";
    private final static String UID_MES = "03-2018";


    public Listas(Context context) {


        nodoUno(UID_MATCH, UID_MES);
        nodoDos(UID_MATCH, UID_MES);

        ctx = context;

    }

    private void nodoUno(final String uid, final String mes) {


        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        nUno = reference.child(NOMBRE_NODO).child(uid);
        nUno.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        Log.i(Constans.CLOUD_STORAGE, "key: " + issue.getKey());

                        NodoUno listado_nodoUno = issue.getValue(NodoUno.class);

                        Log.i(Constans.NODO_UNO, "" + issue.getKey());

                        String llave = issue.getKey();


                        intraNodoUno(uid, llave, mes);


                        Log.d(Constans.TODAS_LAS_LLAVES, "\n" + llave);

                    }


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i(Constans.CLOUD_STORAGE, "se cancelo");
            }
        });


    }

    private void nodoDos(String uid, String mes) {

    }

    private void intraNodoUno(String uid, final String llave, String mes) {


        new Listas.GetDataFromFirebase().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


        // Read from the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(NOMBRE_NODO).child(uid).child(llave);


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                //Log.d(Constans.NODO_UNO, "" + dataSnapshot);

                for (DataSnapshot detalle : dataSnapshot.getChildren()) {


                    NodoUno listado = detalle.getValue(NodoUno.class);


                    if (listado != null && !listado.getFoto().contains("https://firebasestorage.googleapis")) {
                        Log.d(Constans.NODO_UNO, "\n------------------->");
                        Log.d(Constans.NODO_UNO, "\n" + detalle.getKey());
                        Log.d(Constans.NODO_UNO, "\n" + listado.getFecha());
                        Log.d(Constans.NODO_UNO, "\n" + listado.getFoto());
                        Log.d(Constans.NODO_UNO, "\n------------------->");


                        insertarStorage(llave, UID_MES, listado.getFecha(), listado.getFoto());

                    }
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void insertarStorage(final String codigoContador, final String num_mes, final long fecha, String stringbase64) {


        // final String pureBase64Encoded = stringbase64.substring(stringbase64.indexOf(",")  + 1);
        //  final byte[] decodedBytes = Base64.decode(pureBase64Encoded, Base64.DEFAULT);

//        Bitmap decodedImage = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);


        final String pureBase64Encoded = stringbase64.substring(stringbase64.indexOf(",") + 1);
        final byte[] decodedBytes = Base64.decode(pureBase64Encoded, Base64.DEFAULT);

        Bitmap decodedImage = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);


        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/alz");
        myDir.mkdirs();

        String fname = codigoContador + UID_MES + ".jpg";
        File file = new File(myDir, fname);
        Log.i(Constans.NODO_UNO, "" + file);
        if (file.exists())
             file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);
                decodedImage.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();


            } catch (Exception e) {
                Log.e(Constans.NODO_UNO, "" + e);
            }

        // Uri uri = Uri.fromFile(file);

     //   Uri uri = Uri.fromFile(new File(root + "/alz/" + codigoContador + UID_MES + ".jpg"));

        //String db_path = "/storage/emulated/0/alz/" + codigoContador + UID_MES + ".jpg";

        Intent i = new Intent(ctx, ServicioScript.class);
        //i.putExtra("data", db_path); // enviar uri
        i.putExtra("codigocontador", codigoContador); // enviar codigo de contador.
        i.putExtra("fechalong", fecha); // enviar codigo de contador.
        i.putExtra("key_uid", UID_MATCH); // enviar uid del nodo a insertar
        ctx.startService(i);


    }

    private void writeNewContadorFotoslecturas(String url, String codigoContador, long fecha) {
        DatabaseReference mDatabaseLecturas;
        mDatabaseLecturas = FirebaseDatabase.getInstance().getReference(); // referencia a firebase realtime

        Imagen imagen = new Imagen(fecha, url);
        mDatabaseLecturas.child(NOMBRE_NODO).child(UID_MATCH).child(codigoContador).child(UID_MES).setValue(imagen); //referencia a nodo de inserción


        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference(); // referencia a firebase realtime

        Imagen imagenFotos = new Imagen(fecha, url);
        mDatabase.child(NOMBRE_NODO).child(UID_MATCH).child(codigoContador).child(UID_MES).setValue(imagenFotos); //referencia a nodo de inserción


    }


    @SuppressLint("StaticFieldLeak")
    private class GetDataFromFirebase extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }

    }


}
