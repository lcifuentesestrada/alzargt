package com.terabit.alzargt.script;



public class NodoUno {

    private Long fecha;
    private long fecha_pasada;
    private String foto;
    private Long lectura;
    private long lectura_pasada;


    public NodoUno() {
    }

    public NodoUno(Long fecha, long fecha_pasada, String foto, Long lectura, long lectura_pasada) {
        this.fecha = fecha;
        this.fecha_pasada = fecha_pasada;
        this.foto = foto;
        this.lectura = lectura;
        this.lectura_pasada = lectura_pasada;
    }

    public Long getFecha() {
        return fecha;
    }

    public void setFecha(Long fecha) {
        this.fecha = fecha;
    }

    public long getFecha_pasada() {
        return fecha_pasada;
    }

    public void setFecha_pasada(long fecha_pasada) {
        this.fecha_pasada = fecha_pasada;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Long getLectura() {
        return lectura;
    }

    public void setLectura(Long lectura) {
        this.lectura = lectura;
    }

    public long getLectura_pasada() {
        return lectura_pasada;
    }

    public void setLectura_pasada(long lectura_pasada) {
        this.lectura_pasada = lectura_pasada;
    }
}
